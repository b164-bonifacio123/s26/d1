//Create a basic server application by loading NODE js modules

//module it is a software component or part of a program that contains one or more routines

//http => Hyper Text Transfer Protocol
//HTTP is a module to node that transfer data. It is a set of individual files that contain code to create a component that helps establish data transfer between appllications
//Clients(browser) and servers(node js/express) communicate by exchanging individual messages.
//The messages sent by the client, usually a web browser, are called REQUEST.
//The messages sent by the server as an answer are called RESPONSES.

//require() function allows us to use built in modules.
let http = require("http");

//A port is a virtual point where network connections start and end.
//Each port is associated with a specific process or service
//The server will be assigned to port 4000 via the listen(4000) method where the server will listen to any requests that are sent to our server.
//http://localhost:4000
http.createServer(function(request, response) {

	//use the writeHead method to
	//set a status code for the response - a 200 means OK
	//set the content-type
	response.writeHead(200, {'Content-Type': 'application/json'});//text/pain or image/jpeg

	//end() to end the response process
	response.end("Hello World")


}).listen(4000);

//When server is running, console will print the message:
console.log('Server running at localhost:4000')

//status code
//100-199 => Informational responses
//200-299 => Successful responses
//300-399 => Redirection messages
//400-499 => client error
//500-599 => server error responses


//Type node index.js if we want to run/execute the code
//type cntrl C to stop/end the running of code

//nodemon
//it is a package that allow the server to automatically restart when files have been changed for update.
//This will allow us to hot reload the application meaning any changes applied to the application will be applied without having to restart it

//install
//npm install -g nodemon
//or sudo npm install nodemon -g

//to run using nodemon
//nodemon filename
//or npx nodemon filename








