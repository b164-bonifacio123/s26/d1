const http = require('http');

const port = 4000;

//creates a variable "server" that stores the output of the createServer method
const server = http.createServer((request, response) => {

	//to access the /greeting (routes) we will use the request object 
	//"request" is an object that is sent via the client
	//the 'url' property refers to the url or the link or the routes in the client
	//http://localhost:4000/greeting
	//  "/greeting" => this is the routes that the ".url" reads.
	if(request.url == '/greeting') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Hello Again')
	}

	//Mini Activity
	//1. Access the route "/homepage" and return a message of "This is the Home Page! Welcome!"
	//2. Access the route "/about" and return a message of "Welcome to about page!"
	//3. If all other routes is not available, return a message of "Page not found" and a status code of 404

	else if(request.url == '/homepage') {
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('This is the home page. Welcome')
	}

	else if(request.url == '/about'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to about page')
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not found')
	}


})



//Use the server and port variable
server.listen(port);

//When server is running, print this:
console.log(`Server now accessible at localhost:${port}.`);




/*
1. What directive is used by Node.js in loading the modules it needs?
require()

2. What Node.js module contains a method for server creation?
http module

3. What is the method of the http object responsible for creating a server using Node.js?
createServer()

4. What method of the response object allows us to set status codes and content types?
writeHead()

5. Where will console.log() output its contents when run in Node.js?
terminal


6. What property of the request object contains the address's endpoint?
url

*/







